const { description } = require('../../package')
const { backToTopPlugin } = require('@vuepress/plugin-back-to-top');
const { mediumZoomPlugin } = require('@vuepress/plugin-medium-zoom');
const { searchPlugin } = require('@vuepress/plugin-search')
const { defaultTheme } = require('@vuepress/theme-default')


module.exports = {
  lang: 'fr-FR',
  title: 'Nexus blog',
  description: description,
  head: [
    ['meta', { name: 'theme-color', content: '#4295f5' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],
  theme: defaultTheme({
    repo: 'https://gitlab.com/Chiheb-Nexus/chiheb-nexus.gitlab.io',
    editLink: false,
    docsDir: '',
    editLinkText: '',
    lastUpdatedText: 'Dernière mise à jour',
    contributors: false,
    tip: ' ',
    navbar: [
      {
        text: 'Acceuil',
        link: '/',
      },
      {
        text: 'Articles',
        link: '/articles/'
      },
      {
        text: 'À propos',
        link: '/apropos/'
      }
    ],
    sidebar: {
      '/articles/': [
        {
          text: 'Python',
          collapsible: true,
          children: [
            'python-multi-threading-multiprocessing',
            'fullstack-python-web-app-flask-react-webpack',
          ]
        }
      ],
    }
  }),
  plugins: [
    backToTopPlugin,
    mediumZoomPlugin,
    searchPlugin,
  ],
  dest: 'public/',
}
